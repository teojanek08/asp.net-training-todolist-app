namespace ToDoList.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using ToDoList.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<ToDoList.Models.TodoListDBContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(ToDoList.Models.TodoListDBContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            context.TodoList.AddOrUpdate(i => i.Task,
            new TodoList
            {
                User = "Jane",
                Task = "Create task list",
                Priority = 1,
                StartDate = DateTime.Parse("2014-4-24"),
                EndDate = DateTime.Parse("2014-4-25"),
                Status = "Done"
            },

             new TodoList
             {
                User = "Jane",
                Task = "Create another task",
                Priority = 2,
                StartDate = DateTime.Parse("2014-4-24"),
                EndDate = DateTime.Parse("2014-4-25"),
                Status = "Ongoing"
             }

          );
        }
    }
}
