namespace ToDoList.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class User : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TodoLists", "User", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.TodoLists", "User");
        }
    }
}
