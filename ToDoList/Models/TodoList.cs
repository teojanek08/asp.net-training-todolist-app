﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;


namespace ToDoList.Models
{

    public enum TaskStatus
    {
        Canceled,
        Done,
        Delayed,
        Ongoing,
    }

    public class TodoList
    {
        public int ID { get; set; }
        public string Task { get; set; }
        public int Priority { get; set; }

        [Display(Name = "Start Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime StartDate { get; set; }

        [Display(Name = "End Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime EndDate { get; set; }
        public String Status { get; set; }
        public string User { get; set; }


    }

    public class TodoListDBContext : DbContext
    {
        public DbSet<TodoList> TodoList { get; set; }
    }
}