﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ToDoList.Models;

namespace ToDoList.Controllers
{
    public class ToDoListController : Controller
    {
        private TodoListDBContext db = new TodoListDBContext();

        // GET: /ToDoList/
        public ActionResult Index(string sortOrder, string searchString)
        {
            var tasks = from s in db.TodoList
                        select s;

            if (!String.IsNullOrEmpty(searchString))
            {
                tasks = tasks.Where(s => s.Task.Contains(searchString));
            }
            else {
                ViewBag.PrioritySortParam = String.IsNullOrEmpty(sortOrder) ? "Priority" : "";
                ViewBag.DateSortParm = sortOrder;

                switch (sortOrder)
                {
                    case "Priority":
                        tasks = tasks.OrderBy(s => s.Priority);
                        break;
                    case "End Date":
                        tasks = tasks.OrderBy(s => s.EndDate);
                        break;
                    case "Start Date":
                    default:
                        tasks = tasks.OrderBy(s => s.StartDate);
                        break;
                }            
            }
            
            return View(tasks.ToList());
        }

        // GET: /ToDoList/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TodoList todolist = db.TodoList.Find(id);
            if (todolist == null)
            {
                return HttpNotFound();
            }
            return View(todolist);
        }

        // GET: /ToDoList/Create
        public ActionResult Create()
        {
            ViewBag.TaskStatus = SelectTaskStatus();
            return View();
        }

        // POST: /ToDoList/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Task,Priority,StartDate,EndDate,Status,User")] TodoList todolist, string TaskStatus)
        {
            if (ModelState.IsValid)
            {
                todolist.Status = TaskStatus;
                db.TodoList.Add(todolist);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(todolist);
        }

        // GET: /ToDoList/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TodoList todolist = db.TodoList.Find(id);
            if (todolist == null)
            {
                return HttpNotFound();
            }
            ViewBag.TaskStatus = SelectTaskStatus();
            return View(todolist);
        }

        // POST: /ToDoList/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Task,Priority,StartDate,EndDate,Status,User")] TodoList todolist, string TaskStatus)
        {
            if (ModelState.IsValid)
            {
                todolist.Status = TaskStatus;
                db.Entry(todolist).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(todolist);
        }

        // GET: /ToDoList/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TodoList todolist = db.TodoList.Find(id);
            if (todolist == null)
            {
                return HttpNotFound();
            }
            return View(todolist);
        }

        // POST: /ToDoList/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TodoList todolist = db.TodoList.Find(id);
            db.TodoList.Remove(todolist);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public IEnumerable<SelectListItem> SelectTaskStatus()
        {
            IEnumerable<TaskStatus> values = Enum.GetValues(typeof(TaskStatus)).Cast<TaskStatus>();
            IEnumerable<SelectListItem> items = 
                from value in values
                select new SelectListItem
                {
                    Text = value.ToString(),
                    Value = value.ToString(),
                    Selected = value == TaskStatus.Ongoing,
                };

            return items;
        }
    }
}
